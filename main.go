package main

import (
	"bytes"
	"encoding/json"
	"image"
	"image/jpeg"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/dhowden/tag"
	"github.com/disintegration/imaging"
	"github.com/gorilla/mux"
	"github.com/kardianos/osext"
	"github.com/mewkiz/flac"
	"github.com/mewkiz/flac/meta"
)

var (
	// Initialization of the working directory. Needed to load asset files.
	_ = determineWorkingDirectory()
	// File names for the HTTPS certificate
	nocover = "nocover.png"
)

func determineWorkingDirectory() string {
	// Get the absolute path this executable is located in.
	executablePath, err := osext.ExecutableFolder()
	if err != nil {
		log.Fatal("Error: Couldn't determine working directory: " + err.Error())
	}
	// Set the working directory to the path the executable is located in.
	os.Chdir(executablePath)
	return ""
}

var firebaseClient = &http.Client{Timeout: 10 * time.Second}

func getJSON(url string, target interface{}) error {
	r, err := firebaseClient.Get(url)
	if err != nil {
		return err
	}

	defer r.Body.Close()

	return json.NewDecoder(r.Body).Decode(target)
}

type targetFile struct {
	Filename string `json:"filename"`
	Source   string `json:"source"`
}

func check(e error) {
	if e != nil {
		log.Fatal(e)
	}
}

func resizeIMGByte(img []byte, size int) image.Image {

	in := bytes.NewReader(img)
	i, err := imaging.Decode(in)
	if err != nil {
		log.Fatal(err)
	}

	i = imaging.Resize(i, size, size, imaging.Lanczos)

	return i
}

func resizeIMG(img image.Image, size int) image.Image {

	i := imaging.Resize(img, size, size, imaging.Lanczos)

	return i
}

func returnCover(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	size, err := strconv.Atoi(vars["size"])
	if err != nil {
		size = 500
	} else if size > 750 {
		size = 750
	}

	log.Print(size)

	targetFile1 := new(targetFile)
	getJSON("https://fillydelphia-radio.firebaseio.com/rest/fillydelphia-radio/now-playing.json", targetFile1)

	phold, err := imaging.Open(nocover)
	if err != nil {
		log.Print("Couldn't open this file?")
	}

	if targetFile1.Source == "music" || targetFile1.Source == "id" {

		file := strings.Replace(targetFile1.Filename, "&#39;", "'", -1)
		//file := "/home/liquidsoap/radio/audio/music/BroniKoni - Rebirth - 11 Runaway Princess.flac"
		//file := "/home/liquidsoap/radio/audio/music/&I and Adgee - Magic Dance.flac"
		//file := "/home/liquidsoap/radio/audio/music/06 MandoPony & Silva Hound - Wild Fire EP - Wild Fire (Aftermath Remix).mp3"

		f, err := os.Open(file)
		if err != nil {
			w.Header().Set("Content-Type", "image/png")
			i := resizeIMG(phold, int(size))
			jpeg.Encode(w, i, nil)
			log.Print("The file is missing!")
		} else {
			defer f.Close()

			m, errtag := tag.ReadFrom(f)
			if errtag != nil {
				log.Fatal(err)
				w.Header().Set("Content-Type", "image/png")
				i := resizeIMG(phold, int(size))
				jpeg.Encode(w, i, nil)
				log.Print("The tag is invalid!")
			}
			log.Print(m.Format())
			log.Print(targetFile1.Filename)

			if m.Format() == "VORBIS" {
				stream, err := flac.ParseFile(file)
				if err != nil {
					log.Fatal(err)
					w.Header().Set("Content-Type", "image/png")
					i := resizeIMG(phold, int(size))
					jpeg.Encode(w, i, nil)
					log.Print("The tag is invalid!")
				}
				defer stream.Close()
				log.Print("This is a vorbis file")
				pflag := false
				for _, block := range stream.Blocks {
					if block.Type == meta.TypePicture {
						pic := block.Body.(*meta.Picture)
						img := pic.Data
						pflag = true
						log.Print("There is an image VORBIS")
						i := resizeIMGByte(img, int(size))
						jpeg.Encode(w, i, nil)
						break
					}
				}
				if !pflag {
					w.Header().Set("Content-Type", "image/png")
					i := resizeIMG(phold, int(size))
					jpeg.Encode(w, i, nil)
					log.Print("There was no img VORBIS")
				}
			} else {
				log.Print("Not vorbis...")
				log.Print(m.Picture())

				log.Printf("m.Picture is a %T", m.Picture())

				if m.Picture() != nil {
					log.Print("We found an image")
					i := resizeIMGByte(m.Picture().Data, int(size))
					jpeg.Encode(w, i, nil)
				} else {
					w.Header().Set("Content-Type", "image/png")
					i := resizeIMG(phold, int(size))
					jpeg.Encode(w, i, nil)
					log.Print("There was no img IDv3")
				}
			}
		}
	} else {
		w.Header().Set("Content-Type", "image/png")
		i := resizeIMG(phold, int(size))
		jpeg.Encode(w, i, nil)
		log.Print("There was a DJ Live")
	}
	//w.Write([]byte(targetFile1.Filename))
}

func main() {
	myRouter := mux.NewRouter().StrictSlash(true)
	myRouter.HandleFunc("/{size}", returnCover)
	myRouter.HandleFunc("/", returnCover)
	if err := http.ListenAndServe(":9990", myRouter); err != nil {
		panic(err)
	}
}
